# how to setup a vue.js app through node.js
There are various ways to setup a vue application including using the vue-cli or plain node.js. Various approaches has its strength and weakness though, choose it with care :)

## approach: vue cli 
* installation of vue-cli -> https://cli.vuejs.org/guide/installation.html
```javascript:
npm install -g @vue/cli
# OR
yarn global add @vue/cli
```
* verification -> once installed run the following to check for the vue-cli's version
```javascript:
vue --version
```
-----
### version 4.x, 3.x
To create a project simply run:
```javascript:
vue create {{project-name}}
```

### version 2.x
For version 2.x, you would need to install the cli-init package first:
```javascript:
npm install -g @vue/cli-init
```
To create a project simply run:
```javascript:
vue init webpack {{project-name}}
```

-----
### troubleshoot
* npm -> if the following exception happened after running "npm install" or "npm init":
```javascript:
internal/modules/cjs/loader.js:583
    throw err;
    ^

Error: Cannot find module '../lib/utils/unsupported.js'
    at Function.Module._resolveFilename (internal/modules/cjs/loader.js:581:15)
    at Function.Module._load (internal/modules/cjs/loader.js:507:25)
    at Module.require (internal/modules/cjs/loader.js:637:17)
    at require (internal/modules/cjs/helpers.js:22:18)
    at /usr/local/lib/node_modules/npm/bin/npm-cli.js:19:21
    at Object.<anonymous> (/usr/local/lib/node_modules/npm/bin/npm-cli.js:152:3)
    at Module._compile (internal/modules/cjs/loader.js:689:30)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:700:10)
    at Module.load (internal/modules/cjs/loader.js:599:32)
    at tryModuleLoad (internal/modules/cjs/loader.js:538:12)
```
you probably need to re-install node. For Mac, the instruction is here: https://stackabuse.com/how-to-uninstall-node-js-from-mac-osx/.

Next is to install node again, instructions here: https://nodejs.org/en/download/. After installation, the following information should be available and you might need to add back the following paths back to __.bashrc__.
```
- Node.js v14.16.1 to /usr/local/bin/node
- npm v6.14.12 to /usr/local/bin/npm
```

* access rights -> Error: EACCES: permission denied, access '/usr/local/lib/node_modules'
if you encounter this exception; that means root access is required for the operation hence do add __sudo__ infront of the operation and execute again. (etc sudo npm install -g @vue/cli)
