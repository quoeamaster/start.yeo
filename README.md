# start.yeo - a place where starting a new webapp or application dev is less headache.

As development frameworks kept evolving, it might be a headache to setup a brand new project. Seriously the *"familiar"* methods which used to work... suddenly became *"unfamiliar"* after a few months. Hence this repository acts as a place for documenting how to build such projects based on different versions available in the market.

